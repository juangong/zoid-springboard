const path = require('path');

module.exports = env => {

  return ({
    entry: {
      parent: `./component/parent.js`,
    },
    output: {
      path: path.resolve(__dirname, `./dist`),
      filename: '[name].js'
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
      },
    },
  })
};