import zoid from 'zoid'

const element = 'zoid-component'
const options = {}

const springboard = zoid.create({
    tag: 'springboard-frame', // This has to be unique per js loaded on the page
    url: 'https://springboard.ddev.local:8444/', // This should be the url of the webform you want as an iframe
    dimensions: {
        width: '600px',
        height: '2120px'
    },
})

springboard.render(options, element)
